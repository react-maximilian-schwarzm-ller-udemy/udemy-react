import React, { useState } from "react";
import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = ({ onAddExpense }) => {
  const [openForm, setOpenForm] = useState(false);

  const onSaveExpanseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };

    onAddExpense(expenseData);
  };

  const onOpenFormHandler = () => {
    setOpenForm(true);
  };

  const onCloseFormHandler = () => {
    setOpenForm(false);
  }


  return (
    <>
      <div className="new-expense">
        {!openForm && (
          <button type="button" onClick={() => onOpenFormHandler()}>
            Add new expense
          </button>
        )}

        {openForm && (
          <ExpenseForm onSaveExpanseData={onSaveExpanseDataHandler} onCloseForm={onCloseFormHandler}/>
        )}
      </div>
    </>
  );
};

export default NewExpense;
