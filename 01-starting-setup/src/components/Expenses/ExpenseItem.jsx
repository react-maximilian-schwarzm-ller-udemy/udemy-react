import React, { useState } from "react";
import ExpenseDate from "./ExpenseDate";
import Card from "./../UI/Card";
import "./ExpenseItem.css";

const ExpenseItem = ({ item }) => {
  const [title, setTitle] = useState(item.title);

  const clickHandler = () => {
    setTitle("Updated");
  };
  return (
    <li>
      <Card className="expense-item">
        <ExpenseDate data={item.date} />
        <div className="expense-item__description">
          <h2>{title}</h2>
          <div className="expense-item__price">${item.amount}</div>
          <button onClick={clickHandler}>Change title</button>
        </div>
      </Card>
    </li>
  );
};

export default ExpenseItem;
